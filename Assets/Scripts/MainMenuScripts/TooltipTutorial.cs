using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipTutorial : MonoBehaviour
{
    [SerializeField] private GameObject _tooltip;
    [SerializeField] private float _offsetX;
    [SerializeField] private float _offsetY;
    private Vector2 _mousePosition;

    private void Start()
    {
        _tooltip.SetActive(false);
    }

    private void Update()
    {
        _mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    private void OnMouseOver()
    {
        _tooltip.transform.position = _mousePosition + new Vector2(_offsetX, _offsetY);
        _tooltip.SetActive(true);
    }

    private void OnMouseExit()
    {
        _tooltip.SetActive(false);
    }
}
