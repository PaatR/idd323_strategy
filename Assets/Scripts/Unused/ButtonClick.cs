using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonClick : MonoBehaviour
{
    [SerializeField] LayerMask attackButtonLayer;
    [SerializeField] LayerMask passButtonLayer;
    public bool attackButtonClicked;
    public bool passButtonClicked;

    public void AttackAndPassButtonClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Physics2D.OverlapPoint(mousePosition, attackButtonLayer))
            {
                attackButtonClicked = true;
                //Debug.Log(attackButtonClicked);
            }
            else if (Physics2D.OverlapPoint(mousePosition, passButtonLayer))
            {
                passButtonClicked = true;
                //Debug.Log(passButtonClicked);
            }
        }
    }
}
