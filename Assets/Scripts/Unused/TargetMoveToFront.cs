using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMoveToFront : MonoBehaviour
{
    [SerializeField] Transform spawnPoint0;
    [SerializeField] Transform spawnPoint1;
    [SerializeField] Transform spawnPoint2;

    [SerializeField] LayerMask targetLayer;

    string targetTag = "Target";

    public void MoveTargetToFront()
    {
        //CHECK IF THERE IS NO TARGET AT SP1
        if (!Physics2D.OverlapCircle(spawnPoint1.position, .2f, targetLayer))
        {
            //CHECK IF THERE IS A TARGET AT SP2
            if (Physics2D.OverlapCircle(spawnPoint2.position, .2f, targetLayer))
            {
                //GET OBJECT WITH TARGET TAG
                GameObject[] targetSP2List = GameObject.FindGameObjectsWithTag(targetTag);
                foreach (GameObject targetSP2 in targetSP2List)
                    //MOVE IT TO SP1
                    if (targetSP2.transform.position == spawnPoint2.transform.position)
                    {
                        //targetSP2.transform.position = Vector3.MoveTowards(targetSP2.transform.position, spawnPoint1.position, 5 * Time.deltaTime);
                        targetSP2.transform.position = spawnPoint1.transform.position;
                    }
            }
        }

        //CHECK IF THERE IS NO TARGET AT SP0
        if (!Physics2D.OverlapCircle(spawnPoint0.position, .2f, targetLayer))
        {
            //CHECK IF THERE IS A TARGET AT SP1
            if (Physics2D.OverlapCircle(spawnPoint1.position, .2f, targetLayer))
            {
                //GET OBJECT WITH TARGET TAG
                GameObject[] targetSP1List = GameObject.FindGameObjectsWithTag(targetTag);
                foreach (GameObject targetSP1 in targetSP1List)
                    //MOVE IT TO SP0
                    if (targetSP1.transform.position == spawnPoint1.transform.position)
                    {
                        targetSP1.transform.position = spawnPoint0.transform.position;
                    }
            }
        }
    }
}
