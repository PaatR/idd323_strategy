using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MilestoneDisplay : MonoBehaviour
{
    //MILESTONE OBJECTS
    [SerializeField] private SpriteRenderer goal_1;
    [SerializeField] private SpriteRenderer goal_2;
    [SerializeField] private SpriteRenderer goal_3;
    [SerializeField] private SpriteRenderer goal_4;
    [SerializeField] private SpriteRenderer goal_2_line;
    [SerializeField] private SpriteRenderer goal_3_line;
    [SerializeField] private SpriteRenderer goal_4_line;
    [SerializeField] private TextMeshPro goal_1_text;
    [SerializeField] private TextMeshPro goal_2_text;
    [SerializeField] private TextMeshPro goal_3_text;
    [SerializeField] private TextMeshPro goal_4_text;

    //SCRIPT REFERENCE
    [SerializeField] DataStorage _dataStorage;
    [SerializeField] SFXscript _sfx;

    //COLOR
    [SerializeField] Color32 _base;
    [SerializeField] Color32 _goalReached;

    private bool played1;
    private bool played2;
    private bool played3;
    private bool played4;

    private void Start()
    {
        ResetAllToBaseColor();

        played1 = false;
        played2 = false;
        played3 = false;
        played4 = false;
    }

    private void Update()
    {
        if(_dataStorage.playerFood >= 50)
        {
            ChangeToGoalReachedColor(goal_1);
            ChangeTextToGoalReachedColor(goal_1_text);

            if(played1 == false)
            {
                _sfx.PlayMilestoneReachedSound();
                played1 = true;
            }
        }
        if (_dataStorage.playerFood >= 100)
        {
            ChangeToGoalReachedColor(goal_2);
            ChangeToGoalReachedColor(goal_2_line);
            ChangeTextToGoalReachedColor(goal_2_text);

            if (played2 == false)
            {
                _sfx.PlayMilestoneReachedSound();
                played2 = true;
            }
        }
        if (_dataStorage.playerFood >= 200)
        {
            ChangeToGoalReachedColor(goal_3);
            ChangeToGoalReachedColor(goal_3_line);
            ChangeTextToGoalReachedColor(goal_3_text);

            if (played3 == false)
            {
                _sfx.PlayMilestoneReachedSound();
                played3 = true;
            }
        }
        if (_dataStorage.playerFood >= 500)
        {
            ChangeToGoalReachedColor(goal_4);
            ChangeToGoalReachedColor(goal_4_line);
            ChangeTextToGoalReachedColor(goal_4_text);

            if (played4 == false)
            {
                _sfx.PlayMilestoneReachedSound();
                played4 = true;
            }
        }
    }

    void ResetToBaseColor(SpriteRenderer _spriteRenderer)
    {
        _spriteRenderer.color = _base;
    }

    void ResetTextToBaseColor(TextMeshPro _textmeshpro)
    {
        _textmeshpro.color = _base;
    }

    void ChangeToGoalReachedColor(SpriteRenderer _spriteRenderer)
    {
        _spriteRenderer.color = _goalReached;
    }

    void ChangeTextToGoalReachedColor(TextMeshPro _textmeshpro)
    {
        _textmeshpro.color = _goalReached;
    }

    void ResetAllToBaseColor()
    {
        ResetToBaseColor(goal_1);
        ResetToBaseColor(goal_2);
        ResetToBaseColor(goal_3);
        ResetToBaseColor(goal_4);
        ResetToBaseColor(goal_2_line);
        ResetToBaseColor(goal_3_line);
        ResetToBaseColor(goal_4_line);
        ResetTextToBaseColor(goal_1_text);
        ResetTextToBaseColor(goal_2_text);
        ResetTextToBaseColor(goal_3_text);
        ResetTextToBaseColor(goal_4_text);
    }
}
