using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MessageDisplayer : MonoBehaviour
{
    [SerializeField] GameObject needToDismissSoldierMessage;
    [SerializeField] GameObject winMessage;
    [SerializeField] GameObject loseMessage;
    [SerializeField] GameObject passMessage;
    [SerializeField] GameObject convertMessage;

    float messagePopDuration = 1.25f;

    public void Message_DismissRequired()
    {
        needToDismissSoldierMessage.SetActive(true);
    }

    public void MessageOff_DismissRequired()
    {
        needToDismissSoldierMessage.SetActive(false);
    }

    IEnumerator WinMessagePop()
    {
        winMessage.SetActive(true);
        yield return new WaitForSeconds(messagePopDuration);
        winMessage.SetActive(false);
    }
    IEnumerator LoseMessagePop()
    {
        loseMessage.SetActive(true);
        yield return new WaitForSeconds(messagePopDuration);
        loseMessage.SetActive(false);
    }
    IEnumerator PassMessagePop()
    {
        passMessage.SetActive(true);
        yield return new WaitForSeconds(messagePopDuration);
        passMessage.SetActive(false);
    }

    IEnumerator ConvertMessagePop()
    {
        convertMessage.SetActive(true);
        yield return new WaitForSeconds(messagePopDuration);
        convertMessage.SetActive(false);
    }

    public void Message_Win()
    {
        StartCoroutine(WinMessagePop());
    }

    public void Message_Lose()
    {
        StartCoroutine(LoseMessagePop());
    }

    public void Message_Pass()
    {
        StartCoroutine(PassMessagePop());
    }

    public void Message_Convert()
    {
        StartCoroutine(ConvertMessagePop());
    }
}
