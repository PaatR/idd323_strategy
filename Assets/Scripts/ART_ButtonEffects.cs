using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ART_ButtonEffects : MonoBehaviour
{
    [SerializeField] SpriteRenderer buttonSpriteRenderer;
    [SerializeField] Color32 baseColor;
    [SerializeField] Color32 onMouseColor;
    [SerializeField] Color32 onClickColor;

    private void Start()
    {
        buttonSpriteRenderer.color = baseColor;
    }

    private void OnMouseEnter()
    {
        buttonSpriteRenderer.color = onMouseColor;
    }

    private void OnMouseUp()
    {
        buttonSpriteRenderer.color = onMouseColor;
    }

    private void OnMouseDown()
    {
        buttonSpriteRenderer.color = onClickColor;
        buttonSpriteRenderer.color = baseColor;
    }

    private void OnMouseExit()
    {
        buttonSpriteRenderer.color = baseColor;
    }
}
