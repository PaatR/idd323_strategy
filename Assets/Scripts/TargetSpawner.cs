using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TargetSpawner : MonoBehaviour
{
    [SerializeField] GameObject targetPrefab;
    [SerializeField] Transform spawnPoint1;
    [SerializeField] LayerMask targetLayer;

    [SerializeField] LootSet _lootSet;

    int power;
    string loot;
    int spawnCount = 1;

    //Sprites
    [SerializeField] Sprite _foodplus;
    [SerializeField] Sprite _soldierplus;
    [SerializeField] Sprite _foodnsoldier;
    [SerializeField] Sprite _foodplusnsoldier;
    [SerializeField] Sprite _foodnsoldierplus;
    [SerializeField] Sprite _foodplusnsoldierplus;

    private Sprite _targetSprite;

    public void SpawnEnemy()
    {
        //SETTING UP TARGET ATTRIBUTES
        power = (Random.Range(2, 4) * spawnCount);
        _lootSet.RandomizeNumber();

        //SET LOOT NAME AND SPRITES FOR DISPLAY
        switch (_lootSet.LootList[_lootSet.LootRandom].LootType)
        {
            case lootType.FoodEx:
                loot = "food+";
                _targetSprite = _foodplus;
                break;
            case lootType.SoldierEx:
                loot = "soldier+";
                _targetSprite = _soldierplus;
                break;
            case lootType.FoodAndSoldier:
                loot = "food and soldier";
                _targetSprite = _foodnsoldier;
                break;
            case lootType.FoodExAndSoldier:
                loot = "food+ and soldier";
                _targetSprite = _foodplusnsoldier;
                break;
            case lootType.FoodAndSoldierEx:
                loot = "food and soldier+";
                _targetSprite = _foodnsoldierplus;
                break;
            case lootType.FoodExAndSoldierEx:
                loot = "food+ and soldier+";
                _targetSprite = _foodplusnsoldierplus;
                break;
        }

        //-----SPAWN TARGET AND GIVE THEM THE ATTRIBUTES-----
        //CHECK IF SP1 is free
        if (!Physics2D.OverlapCircle(spawnPoint1.position, .2f, targetLayer))
        {
            //SPAWN PREFAB AT SP1
            GameObject spawntarget = Instantiate(targetPrefab);
            spawntarget.transform.position = spawnPoint1.transform.position;

            //GET 3 TEXT MESH PRO IN THE PREFAB
            TextMeshPro[] spawnTargetComponent;
            spawnTargetComponent = spawntarget.GetComponentsInChildren<TextMeshPro>();

            //ADD TEXT TO EACH TEXT MESH PRO
            spawnTargetComponent[0].text = spawnCount.ToString();
            spawnTargetComponent[1].text = power.ToString();
            spawnTargetComponent[2].text = loot;

            //Get 1 SPRITE RENDERER IN THE PREFAB
            SpriteRenderer spawnTargetSpriteRenderer;
            spawnTargetSpriteRenderer = spawntarget.GetComponentInChildren<SpriteRenderer>();
            spawnTargetSpriteRenderer.sprite = _targetSprite;


            //ADD UP SPAWN COUNT AT THE END
            spawnCount += 1;

            //Debug.Log("Spawned");
        }
    }

    public void DestroyFirstTarget()
    {
        //FIND ALL TARGETS
        GameObject FirstTarget = GameObject.FindGameObjectWithTag("Target");

        //FIND TARGETS ONLY AT SP1 POS
        if (FirstTarget.transform.position == spawnPoint1.position)
        {
            //REKT IT
            //Debug.Log("Destroyed");
            Destroy(FirstTarget);
        }
    }
}
