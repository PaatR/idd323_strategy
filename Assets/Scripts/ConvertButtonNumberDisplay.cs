using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ConvertButtonNumberDisplay : MonoBehaviour
{
    [SerializeField] DataStorage _dataStorage;

    private TextMeshPro _convertTextMeshPro;

    void Start()
    {
        _convertTextMeshPro = GetComponentInChildren<TextMeshPro>();
    }

    void Update()
    {
        _convertTextMeshPro.text = $"Ultimate ({_dataStorage._convertLimit})";
    }
}
