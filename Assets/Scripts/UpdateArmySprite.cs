using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateArmySprite : MonoBehaviour
{
    [SerializeField] DataTracker _dataTracker;
    [SerializeField] SpriteRenderer _armyRenderer;

    //SPRITES
    [SerializeField] Sprite _army1;
    [SerializeField] Sprite _army2;
    [SerializeField] Sprite _army3;
    [SerializeField] Sprite _army4;
    [SerializeField] Sprite _army5;
    [SerializeField] Sprite _army6;
    [SerializeField] Sprite _army7;

    void Update()
    {
        switch (_dataTracker.soldierList.Length)
        {
            case 1:
                _armyRenderer.sprite = _army1;
                break;
            case 2:
                _armyRenderer.sprite = _army2;
                break;
            case 3:
                _armyRenderer.sprite = _army3;
                break;
            case 4:
                _armyRenderer.sprite = _army4;
                break;
            case 5:
                _armyRenderer.sprite = _army5;
                break;
            case 6:
                _armyRenderer.sprite = _army6;
                break;
            case 7:
                _armyRenderer.sprite = _army7;
                break;
        }
    }
}
