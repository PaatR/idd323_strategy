using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootSet : MonoBehaviour
{
    [SerializeField] private List<Loot> _loot;
    public List<Loot> LootList => _loot;

    private int _lootRandom;
    public int LootRandom => _lootRandom;

    public void RandomizeNumber()
    {
        //random from 1-100
        int _lootChance = Random.Range(1, 101);

        Debug.Log($"Your loot number is {_lootChance}");

        //define chance, _lootRandom = 0 - 5 (list)
        if(_lootChance > 0 && _lootChance <= 25)
        {
            _lootRandom = 0;
        }
        else if(_lootChance > 25 && _lootChance <= 50)
        {
            _lootRandom = 1;
        }
        else if (_lootChance > 50 && _lootChance <= 75)
        {
            _lootRandom = 2;
        }
        else if (_lootChance > 75 && _lootChance <= 85)
        {
            _lootRandom = 3;
        }
        else if (_lootChance > 85 && _lootChance <= 95)
        {
            _lootRandom = 4;
        }
        else if (_lootChance > 95 && _lootChance <= 100)
        {
            _lootRandom = 5;
        }
    }

    //DEBUGGING
    /*
    public void FindLootFromRandomizedNumber(int _randomizedNumber)
    {
        switch(_randomizedNumber)
        {
            case 0:
                TranslateLoot(_loot[0].LootType);
                break;
            case 1:
                TranslateLoot(_loot[1].LootType);
                break;
            case 2:
                TranslateLoot(_loot[2].LootType);
                break;
            case 3:
                TranslateLoot(_loot[3].LootType);
                break;
            case 4:
                TranslateLoot(_loot[4].LootType);
                break;
            case 5:
                TranslateLoot(_loot[5].LootType);
                break;
        }
    }

    public void TranslateLoot(lootType _translateLootType)
    {
        if(_translateLootType == lootType.FoodEx)
        {
            Debug.Log($"Player Get {_translateLootType}");
        }
        else if(_translateLootType == lootType.SoldierEx)
        {
            Debug.Log($"Player Get {_translateLootType}");
        }
        else if(_translateLootType == lootType.FoodAndSoldier)
        {
            Debug.Log($"Player Get {_translateLootType}");
        }
        else if(_translateLootType == lootType.FoodExAndSoldier)
        {
            Debug.Log($"Player Get {_translateLootType}");
        }
        else if(_translateLootType == lootType.FoodAndSoldierEx)
        {
            Debug.Log($"Player Get {_translateLootType}");
        }
        else if(_translateLootType == lootType.FoodExAndSoldierEx)
        {
            Debug.Log($"Player Get {_translateLootType}");
        }
    }
    */
}
