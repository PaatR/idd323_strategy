using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyDefeatedAmount : MonoBehaviour
{
    [SerializeField] DataStorage _dataStorage;

    private TextMeshPro _enemyDefeatedText;

    private void Start()
    {
        _enemyDefeatedText = GetComponent<TextMeshPro>();
    }

    private void Update()
    {
        _enemyDefeatedText.text = $"you have defeated <color=#FFAE00>{_dataStorage._enemyDefeated}</color> enemies!";
    }
}
