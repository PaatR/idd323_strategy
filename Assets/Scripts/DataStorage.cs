using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataStorage : MonoBehaviour
{
    //-----CURRENT FIRST TARGET PARAMETER-----
    public int firstTargetSpawnCount;
    public int firstTargetPower;
    public string firstTargetLoot;

    //-----PLAYER PARAMETER------
    public float playerWinRate;
    public float playerRawWinRate;
    public int playerFood;
    public int playerPower;
    public int playerConsumption;
    public int _convertLimit;
    public int _enemyDefeated;
}
