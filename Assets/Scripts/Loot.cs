using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Loot
{
    [SerializeField] private lootType _lootType;
    public lootType LootType => _lootType;
}
public enum lootType
{
    FoodEx,
    SoldierEx,
    FoodAndSoldier,
    FoodExAndSoldier,
    FoodAndSoldierEx,
    FoodExAndSoldierEx
}
