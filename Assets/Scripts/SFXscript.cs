using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXscript : MonoBehaviour
{
    [SerializeField] private AudioSource _milestoneReachedSound;
    [SerializeField] private AudioSource _soldierGainSound;
    [SerializeField] private AudioSource _soldierLostSound;

    public void PlayMilestoneReachedSound()
    {
        _milestoneReachedSound.Play();
    }

    public void PlaySoldierGainSound()
    {
        _soldierGainSound.Play();
    }

    public void PlaySoldierLostSound()
    {
        _soldierLostSound.Play();
    }
}
