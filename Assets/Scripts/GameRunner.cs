using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public enum GameState { Spawn, Idle, AttackCalculation, PassCalculation, ConvertCalculation, FirstTargetDeletion}

public class GameRunner : MonoBehaviour
{
    public GameState state;

    //-----SCRIPTS------
    [SerializeField] TargetSpawner targetSpawnerScript;
    [SerializeField] DataTracker dataTrackingScript;
    [SerializeField] DataStorage dataStorage;
    [SerializeField] MessageDisplayer messageDisplayer;
    [SerializeField] LootSet _lootSet;
    [SerializeField] PauseScreen _pauseScreen;
    [SerializeField] SFXscript _sfx;

    //-----NON SCRIPT------
    [SerializeField] LayerMask attackButtonLayer;
    [SerializeField] LayerMask passButtonLayer;
    [SerializeField] LayerMask convertButtonLayer;
    [SerializeField] LayerMask soldierLayer;
    [SerializeField] GameObject loseScreen;
    [SerializeField] LayerMask restartButtonLayer;
    [SerializeField] GameObject winScreen;
    [SerializeField] LayerMask menuButtonLayer;
    [SerializeField] GameObject winButton;
    [SerializeField] LayerMask winButtonLayer;

    private bool _winButtonClicked;

    private void Awake()
    {
        //RESET TIME SCALE AND STORAGE VALUES
        Time.timeScale = 1;
        _pauseScreen.ResetPausing();

        _winButtonClicked = false;
        dataStorage.playerPower = 0;
        dataStorage.playerConsumption = 0;
        dataStorage.playerFood = 0;
        dataStorage.playerFood += 20;
        dataStorage._convertLimit = 2;
        dataStorage._enemyDefeated = 0;

        dataTrackingScript.SpawnFirstSoldier();
    }

    private void Start()
    {
        //START FIRST PHASE
        state = GameState.Spawn;
        SpawnState();
    }

    private void Update()
    {
        //TARGET ATTRIBUTE GET
        dataTrackingScript.GetCurrentTargetAttribute();
        //GET PLAYER PARAMETERS
        dataTrackingScript.PlayerParameterCalculation();
        //SHOW PLAYER PARAMETERS
        dataTrackingScript.PlayerParameterDisplay();
        //BUTTON CLICKING
        IdleStateButtonClick();
        //CHECK IF FOOD IS ENOUGH
        dataTrackingScript.ConsumptionMoreThanFood();
        //CHECK IF CONVERT LIMIT IS REACHED
        dataTrackingScript.ZeroConversionLimit();
        //UPDATE SOLDIER LENGTH
        dataTrackingScript.CheckSoldierListLength();
        //PAUSE KEY AND ENABLER
        _pauseScreen.TogglePause();
        _pauseScreen.ActivatePauseScreen();

        // IF SOLDIER LIST IS 0 THEN LOSE, IF FOOD >= 500 THEN WIN
        if (state == GameState.Idle)
        {
            WinButtonPop();
            PlayerLose();
            PlayerWin();
        }

        //-----DEBUGGING------
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            Debug.Log(state.ToString());
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            dataTrackingScript.LootSoldier(20);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            dataStorage.playerFood += 50;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            dataStorage._convertLimit += 1;
        }
    }

    void SpawnState()
    {
        //Debug.Log("enter spawning process");
        targetSpawnerScript.SpawnEnemy();
        state = GameState.Idle;
        IdleState();
    }

    void IdleState()
    {
        //SOMETHING
    }

    void IdleStateButtonClick()
    {
        //CHECK WHERE MOUSE CURSOR IS
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //IF STATE IS NOT IDLE, DON'T DO ANYTHING
        if (state != GameState.Idle)
        {
            //Debug.Log("Can't click now, not in appropriate state");
            return;
        }

        if (Input.GetMouseButtonDown(1))
        {
            //SOLDIER ICON, RIGHT CLICK TO DISMISS
            if (Physics2D.OverlapPoint(mousePosition, soldierLayer))
            {
                Destroy(Physics2D.OverlapPoint(mousePosition).gameObject);
                _sfx.PlaySoldierLostSound();
            }
        }
        if (Input.GetMouseButtonDown(0) && dataStorage._convertLimit > 0)
        {
            if (Physics2D.OverlapPoint(mousePosition, convertButtonLayer))
            {
                state = GameState.ConvertCalculation;
                ConvertState();
            }
        }

        if (dataStorage.playerConsumption > dataStorage.playerFood)
        {
            messageDisplayer.Message_DismissRequired();
            return;
        }
        if (dataStorage.playerConsumption <= dataStorage.playerFood)
        {
            messageDisplayer.MessageOff_DismissRequired();
        }

        //IF IT'S IDLE, ALLOW BUTTON CLICK
        if (Input.GetMouseButtonDown(0))
        {
            //CHECK BUTTON BY CURSOR POSITION AND LAYERMASK
            //ATTACK BUTTON, CHANGE STATE TO ATTACKCAL(2) <------------------------------- if consumption > food, can't click
            if (Physics2D.OverlapPoint(mousePosition, attackButtonLayer))
            {
                state = GameState.AttackCalculation;
                AttackCalculateState();
            }
            //PASS BUTTON, CHANGE STATE TO PASSCAL(3)
            if (Physics2D.OverlapPoint(mousePosition, passButtonLayer))
            {
                state = GameState.PassCalculation;
                PassCalculateState();
            }
        }
    }

    void AttackCalculateState()
    {
        // CHANGE THIS TO COROUTINE LATER WITH DELAY FOR EACH PROCESS AND INDICATOR FOR THE PLAYER TO SEE.

        //consume food
        dataStorage.playerFood -= dataStorage.playerConsumption;

        //get winning chance
        int getMoreToWin = Random.Range(1, 101);

        //check if win/ if lose
        if (dataStorage.playerRawWinRate >= getMoreToWin)
        {
            //if win, decipher loots and give it to the player
            GainLoot (_lootSet.LootList[_lootSet.LootRandom].LootType);

            //display message
            messageDisplayer.Message_Win();

            //increase defeated enemy amount
            dataStorage._enemyDefeated += 1;

            //delete the first target, change to spawn phase
            state = GameState.FirstTargetDeletion;
            DestroyFirstTarget();
        }
        else if (dataStorage.playerRawWinRate < getMoreToWin)
        {
            //if lose, kill random soldier
            GameObject unluckyLittleSoldier = dataTrackingScript.soldierList[Random.Range(0, dataTrackingScript.soldierList.Length)];
            Destroy(unluckyLittleSoldier);
            _sfx.PlaySoldierLostSound();

            //display message
            messageDisplayer.Message_Lose();

            //delete first target, change to spawn phase
            state = GameState.FirstTargetDeletion;
            DestroyFirstTarget();
        }
    }

    void PassCalculateState()
    {
        //consume food
        dataStorage.playerFood -= dataStorage.playerConsumption;

        //display message
        messageDisplayer.Message_Pass();

        //delete the first target, change to spawn phase
        state = GameState.FirstTargetDeletion;
        DestroyFirstTarget();
    }

    void ConvertState()
    {
        //give loot to the player
        GainLoot(_lootSet.LootList[_lootSet.LootRandom].LootType);

        //display message
        messageDisplayer.Message_Convert();

        //increase defeated enemy amount
        dataStorage._enemyDefeated += 1;

        //reduce convert count
        dataStorage._convertLimit -= 1;

        //delete the first target, change to spawn phase
        state = GameState.FirstTargetDeletion;
        DestroyFirstTarget();
    }

    void DestroyFirstTarget()
    {
        targetSpawnerScript.DestroyFirstTarget();

        state = GameState.Spawn;
        SpawnState();
    }

    void PlayerLose()
    {
        if (dataTrackingScript.soldierList.Length == 0)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            //ENTER LOSE SCREEN
            loseScreen.SetActive(true);

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics2D.OverlapPoint(mousePosition, restartButtonLayer))
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
            }
        }
    }

    void PlayerWin()
    {
        if(_winButtonClicked == true)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            winScreen.SetActive(true);

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics2D.OverlapPoint(mousePosition, restartButtonLayer))
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
                if (Physics2D.OverlapPoint(mousePosition, menuButtonLayer))
                {
                    SceneManager.LoadScene(0);
                }
            }
        }
    }

    void WinButtonPop()
    {
        if (dataStorage.playerFood >= 500)
        {
            winButton.SetActive(true);

            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Input.GetMouseButtonDown(0))
            {
                if (Physics2D.OverlapPoint(mousePosition, winButtonLayer))
                {
                    _winButtonClicked = true;
                }
            }
        }
    }

    void GainLoot(lootType _lootToGain)
    {
        if (_lootToGain == lootType.FoodEx)
        {
            dataStorage.playerFood += dataStorage.firstTargetSpawnCount * 3;
            _sfx.PlaySoldierGainSound();
        }
        else if (_lootToGain == lootType.SoldierEx)
        {
            dataTrackingScript.LootSoldier(2);
        }
        else if (_lootToGain == lootType.FoodAndSoldier)
        {
            dataStorage.playerFood += dataStorage.firstTargetSpawnCount * 2;
            dataTrackingScript.LootSoldier(1);
        }
        else if (_lootToGain == lootType.FoodExAndSoldier)
        {
            dataStorage.playerFood += dataStorage.firstTargetSpawnCount * 3;
            dataTrackingScript.LootSoldier(1);
        }
        else if (_lootToGain == lootType.FoodAndSoldierEx)
        {
            dataStorage.playerFood += dataStorage.firstTargetSpawnCount * 2;
            dataTrackingScript.LootSoldier(2);
        }
        else if (_lootToGain == lootType.FoodExAndSoldierEx)
        {
            dataStorage.playerFood += dataStorage.firstTargetSpawnCount * 3;
            dataTrackingScript.LootSoldier(2);
        }
    }
}
