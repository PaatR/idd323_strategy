using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreen : MonoBehaviour
{
    [SerializeField] private bool _isPaused;

    [SerializeField] private GameObject _pauseDisplay;
    [SerializeField] private LayerMask _resume;
    [SerializeField] private LayerMask _restart;
    [SerializeField] private LayerMask _menu;

    public void TogglePause()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(!_isPaused)
            {
                _isPaused = true;
            }
            else if (_isPaused)
            {
                _isPaused = false;
            }
        }
    }

    public void ActivatePauseScreen()
    {
        if(_isPaused == true)
        {
            _pauseDisplay.SetActive(true);

            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics2D.OverlapPoint(mousePosition, _resume))
                {
                    ResetPausing();
                }
                if (Physics2D.OverlapPoint(mousePosition, _restart))
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
                if (Physics2D.OverlapPoint(mousePosition, _menu))
                {
                    SceneManager.LoadScene(0);
                }
            }
        }
        else
        {
            _pauseDisplay.SetActive(false);
        }
    }

    public void ResetPausing()
    {
        _isPaused = false;
    }
}
