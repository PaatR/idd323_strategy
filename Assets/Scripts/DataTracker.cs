using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DataTracker : MonoBehaviour
{
    [SerializeField] DataStorage dataStorage;
    [SerializeField] SFXscript _sfx;

    //-----CURRENT FIRST TARGET PARAMETER-----
    [SerializeField] Transform spawnPoint1;

    string spawnCountText;
    string powerText;
    string lootText;

    //SOLDIER LIST
    [SerializeField] Transform[] soldierSpawnPointList;     //LIST OF SSP
    [SerializeField] GameObject soldierPrefab;              
    [SerializeField] LayerMask soldierLayer;
    public GameObject[] soldierList;                        //CHECK SOLDIER CURRENT/SOLDIER LIMIT
    int soldierPower;
    int soldierConsumption;

    //-----PLAYER PARAMETER------
    [SerializeField] GameObject _blackBar;
    [SerializeField] GameObject _blackBar2;

    public void GetCurrentTargetAttribute()
    {
        //FIND ALL TARGETS
        GameObject CurrentTarget = GameObject.FindGameObjectWithTag("Target");

        //FIND TARGETS ONLY AT SP0 POS
        if (CurrentTarget.transform.position == spawnPoint1.position)
        {
            //GET 3 TEXT MESH PRO IN THIS PREFAB
            TextMeshPro[] CurrentTargetComponents;
            CurrentTargetComponents = CurrentTarget.GetComponentsInChildren<TextMeshPro>();

            spawnCountText = CurrentTargetComponents[0].text;
            powerText = CurrentTargetComponents[1].text;
            lootText = CurrentTargetComponents[2].text;

            //CHANGE THE TEXT TO NUMBERS
            int.TryParse(spawnCountText, out dataStorage.firstTargetSpawnCount);
            int.TryParse(powerText, out dataStorage.firstTargetPower);
            dataStorage.firstTargetLoot = lootText;
        }
    }

    public void SpawnFirstSoldier()
    {
        //SETTING UP FIRST SOLDIER ATTRIBUTE
        int firstSoldierPower = 5;
        int firstSoldierConsumption = 2;

        //CHECK IF SSP0 IS FREE
        if(!Physics2D.OverlapCircle(soldierSpawnPointList[0].position, .2f, soldierLayer))
        {
            //SPAWN SOLDIER AT SP6
            GameObject spawnFirstSoldier = Instantiate(soldierPrefab);
            spawnFirstSoldier.transform.position = soldierSpawnPointList[0].position;

            //GET 2 TEXT MESH PRO IN SOLDIER
            TextMeshPro[] firstSoldierComponent;
            firstSoldierComponent = spawnFirstSoldier.GetComponentsInChildren<TextMeshPro>();

            //ADD TEXT THERE
            firstSoldierComponent[0].text = firstSoldierPower.ToString();
            firstSoldierComponent[1].text = firstSoldierConsumption.ToString();
        }
    }

    public void CheckSoldierListLength()
    {
        soldierList = GameObject.FindGameObjectsWithTag("Soldier");
    }

    public void LootSoldier(int _multiplier)
    {
        //SETTING UP RANDOM SOLDIER ATTRIBUTE
        soldierPower = (_multiplier * dataStorage.firstTargetSpawnCount);
        soldierConsumption = dataStorage.firstTargetSpawnCount;

        void LootSoldierSpawning(int SSP)
        {
            GameObject spawnLootSoldier = Instantiate(soldierPrefab);
            spawnLootSoldier.transform.position = soldierSpawnPointList[SSP].position;

            TextMeshPro[] lootSoldierComponent;
            lootSoldierComponent = spawnLootSoldier.GetComponentsInChildren<TextMeshPro>();

            lootSoldierComponent[0].text = soldierPower.ToString();
            lootSoldierComponent[1].text = soldierConsumption.ToString();

            _sfx.PlaySoldierGainSound();
        }

        //CHECK FIRST IF THE SOLDIER LIST IS FREE
        if (soldierList.Length < 7)
        {
            //CHECK SPP0 TO SSP6, SPAWN IN FIRST FREE SLOT
            if (!Physics2D.OverlapCircle(soldierSpawnPointList[0].position, .2f, soldierLayer))
            {
                //SPAWN AND STOP
                LootSoldierSpawning(0);
                return;
            }
            if (!Physics2D.OverlapCircle(soldierSpawnPointList[1].position, .2f, soldierLayer))
            {
                LootSoldierSpawning(1);
                return;
            }
            if (!Physics2D.OverlapCircle(soldierSpawnPointList[2].position, .2f, soldierLayer))
            {
                LootSoldierSpawning(2);
                return;
            }
            if (!Physics2D.OverlapCircle(soldierSpawnPointList[3].position, .2f, soldierLayer))
            {
                LootSoldierSpawning(3);
                return;
            }
            if (!Physics2D.OverlapCircle(soldierSpawnPointList[4].position, .2f, soldierLayer))
            {
                LootSoldierSpawning(4);
                return;
            }
            if (!Physics2D.OverlapCircle(soldierSpawnPointList[5].position, .2f, soldierLayer))
            {
                LootSoldierSpawning(5);
                return;
            }
            if (!Physics2D.OverlapCircle(soldierSpawnPointList[6].position, .2f, soldierLayer))
            {
                LootSoldierSpawning(6);
                return;
            }
        }
        else if(soldierList.Length >= 7)
        {
            Debug.Log("SOLDIER LIST FULL");
        }
    }

    public void PlayerParameterCalculation()
    {
        //RESET THE VALUE
        dataStorage.playerPower = 0;
        dataStorage.playerConsumption = 0;

        //FIND AMOUNT OF SOLDIER
        soldierList = GameObject.FindGameObjectsWithTag("Soldier");
        foreach(GameObject CurrentSoldier in soldierList)
        {
            //READ EACH SOLDIER PARAMETER
            TextMeshPro[] SoldierParameter;
            SoldierParameter = CurrentSoldier.GetComponentsInChildren<TextMeshPro>();

            string powerPARAM = SoldierParameter[0].text;
            string foodPARAM = SoldierParameter[1].text;

            //CHANGE TYPE TO NUMBER
            int.TryParse(powerPARAM, out int paramPower);
            int.TryParse(foodPARAM, out int intFood);

            //ADD UP TO THE TOTAL
            dataStorage.playerPower += paramPower;
            dataStorage.playerConsumption += intFood;
        }

        //CALCULATE WIN RATE
        dataStorage.playerRawWinRate = ((((float)dataStorage.playerPower / (float)dataStorage.firstTargetPower) * 10000.00f) / 100.00f);
        //DISPLAY THE WIN RATE BETWEEN 0-100 PERCENT
        dataStorage.playerWinRate = Mathf.Clamp((Mathf.Round(dataStorage.playerRawWinRate * 100.0f) / 100.0f), 0f, 100f);
    }

    public void PlayerParameterDisplay()
    {
        //FIND PLAYER
        GameObject Player = GameObject.Find("Player");
        //GET 4 Text Mesh Pros
        TextMeshPro[] PlayerComponents;
        PlayerComponents = Player.GetComponentsInChildren<TextMeshPro>();

        PlayerComponents[0].text = dataStorage.playerFood.ToString() + $"<color=#C6C6C6> (-{dataStorage.playerConsumption})</color>";
        PlayerComponents[1].text = dataStorage.playerPower.ToString();
        PlayerComponents[2].text = dataStorage.playerConsumption.ToString();
        PlayerComponents[3].text = dataStorage.playerWinRate + "%".ToString();

        if(dataStorage.playerWinRate >= 100)
        {
            PlayerComponents[3].color = new Color32(105, 209, 152, 255);
        }
        if(dataStorage.playerWinRate >= 70 && dataStorage.playerWinRate < 100)
        {
            PlayerComponents[3].color = new Color32(203, 195, 106, 255);
        }
        if (dataStorage.playerWinRate >= 40 && dataStorage.playerWinRate < 70)
        {
            PlayerComponents[3].color = new Color32(202, 157, 106, 255);
        }
        if (dataStorage.playerWinRate < 40)
        {
            PlayerComponents[3].color = new Color32(203, 102, 95, 255);
        }
    }

    public void ConsumptionMoreThanFood()
    {
        //ACTIVATION OF BLACK BAR
        if (dataStorage.playerConsumption > dataStorage.playerFood)
        {
            _blackBar.SetActive(true);
        }
        if (dataStorage.playerConsumption <= dataStorage.playerFood)
        {
            _blackBar.SetActive(false);
        }
    }

    public void ZeroConversionLimit()
    {
        //ACTIVATION OF BLACK BAR 2
        if(dataStorage._convertLimit <= 0)
        {
            _blackBar2.SetActive(true);
        }
        if(dataStorage._convertLimit > 0)
        {
            _blackBar2.SetActive(false);
        }
    }
}
